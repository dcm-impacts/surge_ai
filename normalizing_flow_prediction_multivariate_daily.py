from math import floor, ceil
import warnings
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
import xarray as xr
from gluonts.dataset.common import ListDataset
from gluonts.dataset.util import to_pandas
from gluonts.evaluation import MultivariateEvaluator
from pts.model.deepar import DeepAREstimator
# from sklearn.neighbors import BallTree
from pts.dataset.repository.datasets import dataset_recipes
from gluonts.dataset.repository.datasets import get_dataset
from gluonts.dataset.multivariate_grouper import MultivariateGrouper
from pts.model.time_grad import TimeGradEstimator
import matplotlib.pyplot as plt
import torch
import torch.utils.data as data
import zuko
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.tsa.ar_model import AutoReg
from torch.utils.data import DataLoader, TensorDataset
import pdb

# Initialize lists to store test losses
train_losses = []
test_losses = []
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.5)
optimizer = torch.optim.Adam(flow.parameters(), lr=1e-3)
scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
    optimizer, T_0=10, T_mult=2)
for epoch in range(30):
    losses = []
    grad_norms = []

    # Training loop
    for x, label, _ in train_loader:
        # Move input data and labels to GPU
        x = x.float().to(device)
        label = label.float().to(device)

        # Prepare conditioning variable and move it to the GPU
        c = label

        # Calculate loss, move it to GPU, and perform backward pass
        loss = -flow(c).log_prob(x).mean()
        loss.backward()
        for param in flow.parameters():
            if param.grad is not None:
                grad_norms.append(param.grad.norm().item())
        # Update model weights
        optimizer.step()
        optimizer.zero_grad()

        # Detach loss to avoid accumulating gradients and store it
        losses.append(loss.detach())

    # Convert gradient norms to a tensor to compute mean and std
    grad_norms = torch.tensor(grad_norms)
    grad_mean = grad_norms.mean().item()
    grad_std = grad_norms.std().item()

    # Print mean and std of the gradient norms
    print(f"Epoch {epoch}: Gradient Norms Mean = {
          grad_mean:.6f}, Std = {grad_std:.6f}")

    scheduler.step()  # Step the scheduler at the end of each epoch

    # Stack the losses and print mean and standard deviation
    losses = torch.stack(losses)
    train_loss_mean = losses.mean().item()
    train_loss_std = losses.std().item()
    train_losses.append(train_loss_mean)

    print(f'({epoch}) Train Loss: {
          train_loss_mean:.6f} ± {train_loss_std:.6f}')

    # Test loop (evaluation mode)
    flow.eval()  # Set model to evaluation mode
    test_loss = []
    with torch.no_grad():  # Disable gradient calculations
        for x_test, label_test, _ in test_loader:
            # Move input data and labels to GPU
            x_test = x_test.float().to(device)
            label_test = label_test.float().to(device)

            # Prepare conditioning variable and move it to the GPU
            c_test = label_test

            # Calculate test loss
            loss_test = -flow(c_test).log_prob(x_test).mean()
            test_loss.append(loss_test.detach())

    test_loss = torch.stack(test_loss)
    test_loss_mean = test_loss.mean().item()
    test_loss_std = test_loss.std().item()
    test_losses.append(test_loss_mean)

    print(f'({epoch}) Test Loss: {test_loss_mean:.6f} ± {test_loss_std:.6f}')

    flow.train()  # Switch back to training mode

# Plot the test loss
plt.figure(figsize=(10, 6))
plt.plot(range(30), test_losses, label='Test Loss')
plt.plot(range(30), train_losses, label='Train Loss')

plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.title('Test Loss over Epochs')
plt.legend()
plt.grid(True)
plt.show()


# In[77]:


# In[78]:



# Now `datetime_array` contains the correct datetime values


# In[79]:


# Step 3: Retrieve the data for those indices from the TensorDataset
selected_target_data = test_dataset[:][0]


# In[80]:


x = date_indices

# In[81]:


# Calculate percentiles for the current slice
# In[82]:


# In[83]:




# In[84]:


bias = (selected_target_data - q50).numpy()

plt.plot(bias)


# In[85]:


num_samples = 1000
# Step 1: Fit the AR(2) model to your surge data using AutoReg
model = AutoReg(bias[:, 0], lags=2).fit()

# Step 2: Get the estimated AR(2) coefficients (phi_1, phi_2)
phi_1, phi_2 = model.params[1:3]  # Ignore the intercept for now

# Step 3: Generate a new AR(2) time series based on the estimated parameters
n = len(x)  # Length of the time series
sigma = np.std(model.resid)  # Standard deviation of the residuals as noise
X = np.zeros((n, num_samples))


# Generate white noise (epsilon) for the new series
epsilon = np.random.normal(0, sigma, (n, num_samples))

# Initialize the first two values (you can set them to 0 or other values)
X[0, :] = np.random.normal(0, sigma, num_samples)
X[1, :] = np.random.normal(0, sigma, num_samples)

# Generate the AR(2) process using the estimated phi_1 and phi_2
for t in range(2, n):
    X[t, :] = phi_1 * X[t-1, :] + phi_2 * X[t-2, :] + epsilon[t, :]
X = X/X[:, 0].std()
# Step 4: Plot the generated AR(2) time series
plt.figure(figsize=(10, 6))
for i in range(num_samples):
    plt.plot(X[:, i], label=f"Generated AR(2) Time Series (phi_1={
             phi_1:.2f}, phi_2={phi_2:.2f})")
    if i == 2:
        break
plt.title("Randomly Generated AR(2) Time Series")
plt.xlabel("Time")
plt.ylabel("Value")
plt.legend()
plt.show()


# In[86]:


z = torch.tensor(
    X, dtype=torch.float32).unsqueeze(-1).expand(-1, -1, n_stations).to(device)

Predicted = generate_surge_distribution(
    flow, selected_predictor_data, num_samples=1, z=z, device=device)


# In[87]:


nrows = ceil(n_stations/4)
if nrows == 1:
    ncols = n_stations
else:
    ncols = 4

fig, axes = plt.subplots(nrows, ncols, figsize=(15, 15))
if n_stations == 1:
    axes = [axes]
else:
    axes = axes.flatten()  # Flatten to easily iterate over axes
idx_sel = slice(0, 365//4)
x_sel = x[idx_sel]
for i in range(n_stations):
    surge_dist_slice = surge_dist[:, :, i]

    ax = axes[i]
    ax.plot(x_sel, q50[idx_sel, i], label='Median', color='C0')

    # Fill 90% Prediction Interval
    ax.fill_between(x_sel, q5[idx_sel, i], q95[idx_sel, i],
                    color='gray', alpha=0.3, label='90% Prediction Interval')

    # Fill 50% Prediction Interval
    ax.fill_between(x_sel, q25[idx_sel, i], q75[idx_sel, i],
                    color='orange', alpha=0.3, label='50% Prediction Interval')

    ax.plot(x_sel, selected_target_data[idx_sel, i], c="black", label='Target')
    ax.plot(x_sel, Predicted[idx_sel, 0, i], c="C2", label="Sample")

    ax.set_title(f'Subplot {i+1}')
    ax.set_xlabel('X-axis label')
    ax.set_ylabel('Y-axis label')
    ax.tick_params(axis='x', rotation=30)

# Adjust layout
fig.tight_layout()

# Optionally, add a single legend for all subplots
handles, labels = ax.get_legend_handles_labels()
fig.legend(handles, labels, loc='upper center', ncol=4)

plt.show()


# In[ ]:


# In[88]:


def print_autocorrelation(time_series):
    # Step 1: Fit the AR(2) model to your surge data using AutoReg
    model = AutoReg(time_series, lags=2).fit()

    # Step 2: Get the estimated AR(2) coefficients (phi_1, phi_2)
    phi_1, phi_2 = model.params[1:3]  # Ignore the intercept for now
    print(f"{phi_1=}, {phi_2=}")


# In[89]:


print_autocorrelation(selected_target_data[:, 0].numpy())


# In[90]:


print_autocorrelation(Predicted[:, 1])


# In[91]:


print_autocorrelation(surge_dist[:, 5])


# In[92]:


selected_target_data[:, 0].numpy().std()


# In[104]:


np.percentile(selected_target_data[:, 0].numpy(), 99.99)


# In[102]:


np.percentile(surge_dist.mean(axis=1), 99.9)


# In[103]:


np.percentile(Predicted[:, 3], 99.9)


# In[96]:


np.array([np.percentile(surge_dist[:, i], 99)
         for i in range(surge_dist.shape[1])]).mean()


# In[97]:


np.array([np.percentile(Predicted[:, i], 99)
         for i in range(Predicted.shape[1])]).mean()


# In[58]:


# Extract the 'target' data
target_data = list(dataset.test)[0]["target"]

# Remove NaN values using NumPy
clean_target_data = target_data[~np.isnan(target_data)]

# Compute the 99.9th percentile
percentile_99_9 = np.percentile(clean_target_data, 99.9)

print(percentile_99_9)


# In[89]:


plt.hist(Predicted[0, :], bins=100, alpha=.5,
         density=True, label="autocorrelated noise")
plt.hist(surge_dist[0, :1000], bins=100, alpha=.5,
         density=True, label="rand_dist")
plt.legend()


# In[326]:


plt.hist(X[0, :], bins=100, density=True, alpha=.5)
plt.hist(np.random.normal(0, 1, 1000), bins=100, density=True, alpha=.5)


# In[327]:


plt.hist(X[:, 0], bins=100, density=True, alpha=.5)
plt.hist(np.random.normal(0, 1, 1000), bins=100, density=True, alpha=.5)


# In[ ]:


# In[ ]:


# In[ ]:


# In[ ]:


# In[ ]:


# In[ ]:


# In[ ]:


def process_train_data(data_list, n_stations, n):
    # Create surge and ps DataFrames
    surge_df = pd.DataFrame(
        {i: to_pandas(data_list[i]) for i in range(n_stations)}).interpolate()
    ps_df = pd.DataFrame(
        {i: to_pandas(data_list[i].copy()) for i in range(n_stations)}).interpolate()

    # Set the index to timestamp
    surge_df.index = surge_df.index.to_timestamp()
    ps_df.index = ps_df.index.to_timestamp()

    for i in range(24):
        # Apply rolling operations on train data
        # todo make shure that the rolling window is only applied such that all values have valid data
        # todo cut off nan values
        # todo make shure that the same indices are cut in surge_df and in ps_df
        surge_df = surge_df.rolling(window=24, step=24).max()
        ps_df = ps_df.rolling(window=24, step=24).mean()

        # Compute mean and std for train data
        train_mean = ps_df.mean()
        train_std = ps_df.std()

        # Preprocess train data using its own mean and std
        ps_df = (ps_df - train_mean) / train_std

        # Create shifted data and PyTorch tensors
        predictor_ps_tensor, predictand_surge_tensor, date_tensor = create_shifted_data_and_tensors(
            ps_df, surge_df, n_stations, n)
        # shift surge_df and ps_df one hour to the right
        # todo append this to a list of all predictor predictand and date tensors
    # todo concat the list of predictor predictand and date tensors

    # Create TensorDataset and DataLoader
    dataset = TensorDataset(predictand_surge_tensor,
                            predictor_ps_tensor, date_tensor)
    loader = DataLoader(dataset, batch_size=512, shuffle=False)

    return dataset, loader, train_mean, train_std
