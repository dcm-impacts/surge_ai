#!/bin/bash

# SLURM Directives
#SBATCH --qos=priority
#SBATCH --partition=priority
#SBATCH --job-name=rechunking
#SBATCH --account=isimip
#SBATCH --output=log/%x_%j.log
#SBATCH --error=log/%x_%j.log
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16

# Load necessary modules or set up the environment
module purge
module load nco # Uncomment and modify if module load is required

# Directories containing the NetCDF files

# Loop through each NetCDF file in the directory
# Check if exactly one argument is passed
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <input_directory> <output_directory>"
    exit 1
fi

# Directories containing the NetCDF files
dir=$1
odir=$2

for ifile in "$dir"/*.nc; do
    # Output file name
    ofile="${ifile%.nc}_rechunked.nc"

    echo "Rechunking file: $ifile"

    # Extract the number of times
    n_times=$(ncks --trd -m -M "$ifile" | grep -E -i ": time, size =" | cut -f 7 -d ' ' | uniq)
    echo "there are $n_times time steps in $ifile"

    # Rechunk the file
    ncks -O --cnk_csh=15000000000 --cnk_plc=g3d --cnk_dmn=time,$n_times --cnk_dmn=latitude,10 --cnk_dmn=longitude,10 "$ifile" "$odir/$(basename $ofile)"
    # ncks -O --cnk_plc=g3d --cnk_dmn=time,$n_times --cnk_dmn=lat,1 --cnk_dmn=lon,1 "$ifile" "$ofile"

    echo "Rechunked file saved as: $ofile"
done

echo "Rechunking process completed."
# todo make this more generic
# todo make this a proper cleanup
rm -r /p/tmp/sitreu/projects/surge_ai/tmp/$(basename $dir)

