#!/bin/bash

#SBATCH --qos=gpushort
#SBATCH --partition=gpu
#SBATCH --account=isimip
#SBATCH --ntasks=1
#SBATCH --time=8:00:00

source ./variables_for_shellscripts.sh 
args="--num_epochs=100 --sequence_length=72 --station_idx=16120 --climate_input=ERA5_2deg"
echo $args
cd $project_basedir
$surge_ai_python -u train.py $args
