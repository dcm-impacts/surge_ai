#!/bin/bash
# This is a wrapper to run slurm tasks

source ./variables_for_shellscripts.sh

logdir=$project_basedir/log
mkdir -p $logdir
logfile=$logdir/%x_%j 
user=$(whoami)

sbatch --mail-user=$user@pik-potsdam.de --output=$logfile --error=$logfile --job-name=surge_ai_01 --account=isimip run_lstm_model.sh

