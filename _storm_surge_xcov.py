# Generate a storm surge dataset.
import os
from functools import lru_cache
from pathlib import Path

import numpy as np
import pandas as pd
import xarray as xr
from gluonts.dataset import DatasetWriter
from gluonts.dataset.common import ListDataset, MetaData, TrainDatasets
from gluonts.dataset.repository._util import metadata


def generate_reanalysis_surge_dataset(
    dataset_path: Path,
    train_data_path: Path,
    test_data_path: Path,
    dataset_writer: DatasetWriter,
    lat_min: float,
    lat_max: float,
    lon_min: float,
    lon_max: float,
    prediction_length: int = 24,
    n_stations: int = None,
    pandas_freq: str = "H",
):
    def get_station_indices_within_bounds(
        file_path, lat_min, lat_max, lon_min, lon_max, n=None
    ):
        with xr.open_dataset(file_path) as ds:
            stations_in_bounds = ds.where(
                (ds.station_y_coordinate >= lat_min)
                & (ds.station_y_coordinate <= lat_max)
                & (ds.station_x_coordinate >= lon_min)
                & (ds.station_x_coordinate <= lon_max),
                drop=True,
            )
            station_indices = stations_in_bounds.stations.values
            if n is not None and n < len(station_indices):
                indices = np.linspace(0, len(station_indices) - 1, n, dtype=int)
                sampled_indices = station_indices[indices]
            else:
                sampled_indices = station_indices
            return sampled_indices

    def load_and_concat_nc_files(directory, stations, file_pattern="*.nc"):
        directory = Path(directory)
        nc_files = sorted(directory.glob(file_pattern))

        def load_file(file):
            return xr.load_dataset(file).sel(stations=stations)

        datasets = []
        for file in nc_files:
            datasets.append(load_file(file))
        combined_dataset = xr.concat(datasets, dim="time")
        return combined_dataset

    # Get station indices within the geographical bounds
    station_indices = get_station_indices_within_bounds(
        train_data_path / Path("reanalysis_surge_hourly_1979_01_v1.nc"),
        lat_min,
        lat_max,
        lon_min,
        lon_max,
        n_stations,
    )

    # Load and concatenate datasets for train and test
    train_surge = load_and_concat_nc_files(train_data_path, station_indices)
    test_surge = load_and_concat_nc_files(test_data_path, station_indices)
    train_ds = []
    test_ds = []
    for station in station_indices:
        for mode in "train", "test":
            time_series = {
                "start": pd.to_datetime(locals()[f"{mode}_surge"].time.values[0]),
                # "station_id": station,
                "target": locals()[f"{mode}_surge"]
                .sel(stations=station)
                .surge.to_numpy(),
            }
            locals()[f"{mode}_ds"].append(time_series.copy())

    # Define metadata
    meta = MetaData(
        **metadata(
            cardinality=[train_surge.surge.shape[1]],
            freq=pandas_freq,
            prediction_length=prediction_length,
        )
    )

    # Save datasets
    dataset = TrainDatasets(metadata=meta, train=train_ds, test=test_ds)
    dataset.save(path_str=str(dataset_path), writer=dataset_writer, overwrite=True)

    print(f"Dataset saved at {dataset_path}")
