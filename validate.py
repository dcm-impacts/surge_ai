import torch
import torch.nn as nn
from torch.utils.data import DataLoader

from surge_ai.dataset import CoastalWaterLevelDataset
from surge_ai.model import SurgePredictorLSTM
from surge_ai.validate import evaluate_model

val_dataset = CoastalWaterLevelDataset(
    "data/val/reanalysis_surge",
    "data/val/ERA5/ps",
    "data/val/ERA5/uas",
    "data/val/ERA5/vas",
    sequence_length=72,
)

# Define validation DataLoader
val_loader = DataLoader(
    dataset=val_dataset, batch_size=32, shuffle=False
)  # Define val_dataset similarly to your training dataset

# Evaluate the model
trained_model = SurgePredictorLSTM(
    input_size=4,
    hidden_size=24,
    num_layers=1,
    prediction_length=1,
    sequence_length=val_dataset.sequence_length,
)
trained_model.load_state_dict(torch.load("surge_predictor_lstm.pth"))

criterion = nn.MSELoss()
validation_loss = evaluate_model(trained_model, val_loader, criterion)
print(f"Validation Loss: {validation_loss}")
