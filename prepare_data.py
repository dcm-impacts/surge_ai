""" This will download and preprocess all data needed for the project """
import argparse
import subprocess
import time
from pathlib import Path

from surge_ai.data.download_water_level import download_codec


def copy_and_preprocess_era5(
    era5_path,
    years,
    vars,
    slurm=True,
):
    """
    Copies ERA5 data files and preprocesses them by
    changing the chunk dimensions to store all time steps for 10 x 10 latitude - longitude chunks.

    Args:
        era5_path (str or Path): Path to the directory containing ERA5 data.
        years (list of int): List of years for which data needs to be processed.
        slurm (bool, optional): If True, uses SLURM for execution. If False, uses bash. Defaults to True.
        vars (list of str, optional): List of variables to process. Defaults to ["ps", "uas", "vas"].

    Raises:
        ValueError: If the input directory for a variable does not exist or if a required file is missing.
        subprocess.CalledProcessError: If the subprocess command fails.

    Notes:
        - Rechunking requires approximately twice the memory of one ERA5 file i.e. 20-40GB of RAM.
        - NCO must be installed on the system to run the rechunk.sh shell script
    """
    if slurm:
        execution_command = "sbatch"
    else:
        execution_command = "bash"
    for var in vars:
        target_dir = Path(f"data/era5/{var}")
        target_dir.mkdir(exist_ok=True, parents=True)
        input_dir = era5_path / var
        if not input_dir.exists():
            raise ValueError(
                f"The directory {input_dir} does not exist. Please verify spelling or download."
            )
        tmp_dir = Path("tmp") / Path(f"tmp_dir_for_rechunking_{var}")
        if tmp_dir.exists():
            raise ValueError(
                "There already is a dir {tmp_dir}. "
                "please ensure if there is another rechunking script running. "
                f"If not recursively remove rm -r {tmp_dir}"
            )
        tmp_dir.mkdir(parents=True)

        try:
            for year in years:
                file_name = (
                    f"{var}_1hr_ECMWF-ERA5_observation_{year}010100-{year}123123.nc"
                )
                file_path = input_dir / file_name
                if file_path.exists():
                    # Creating a symlink for each file in the temporary directory
                    symlink_path = tmp_dir / file_name
                    if (
                        not symlink_path.exists()
                    ):  # Check if symlink already exists to avoid error
                        symlink_path.symlink_to(file_path)
                else:
                    raise ValueError(
                        f"The file {file_name} does not exist. Please download."
                    )

            subprocess.run(
                [
                    execution_command,
                    "preprocessing/rechunk.sh",
                    str(tmp_dir),
                    str(target_dir),
                ],
                check=True,
            )
        except Exception as e:
            # cleanup
            tmp_dir.rmdir()
            raise e


def main(water_level, era5, years, climate_variables):
    """
    Main function to handle the loading and processing of water level and ERA5 data.

    Args:
        water_level (str or Path): Indicates how to handle water level data.
            - If "skip", loading of water level data is skipped.
            - If "download", water level data will be downloaded.
            - If a pathlib.Path object, it indicates the location of the water level data.
        era5 (str or Path): Indicates how to handle ERA5 data.
            - If "skip", loading of ERA5 data is skipped.
            - If "download", downloading ERA5 data is not implemented yet.
            - If a pathlib.Path object, it indicates the location of the ERA5 data.
        years (list of int): List of years for which data needs to be processed.

    Raises:
        NotImplementedError: If the method to download ERA5 data is not implemented,
            or if handling of water level or ERA5 data in a specific manner is not implemented.

    Notes:
        - If 'era5' is set to "download", the function raises NotImplementedError as downloading ERA5 data is not implemented.
    """
    if water_level == "skip":
        print("loading water level data is skipped")
    elif water_level == "download":
        print("start downloading water level data")
        start_time = time.time()
        download_codec(years)
        rounded_duration = round(time.time() - start_time)
        print(
            "water level data has been downloaded.",
            f"The download took {rounded_duration} seconds",
        )
    elif isinstance(water_level, Path):
        raise NotImplementedError(
            "You must implement how the data can be copied",
            "from the specified Path such that it has the correct format",
        )
    else:
        raise NotImplementedError(
            "water_level must be ",
            "'skip', 'download' or a pathlib.Path object ",
            "indicating the location of the water level data",
        )
    # load era5 data
    if era5 == "skip":
        print("loading era5 data is skipped")
    elif era5 == "download":
        raise NotImplementedError("Implement a method to download the ERA5 data")
    elif isinstance(era5, Path):
        copy_and_preprocess_era5(era5, years, vars=climate_variables)
    else:
        raise NotImplementedError(
            "era5 must be ",
            "'skip', 'download' or a pathlib.Path object ",
            "indicating the location of the era5 data",
        )

    # specify if ERA5 needs to be downloaded
    # or if it can be copied from some location


if __name__ == "__main__":
    # Create the argument parser
    parser = argparse.ArgumentParser(description="Download and preprocess data")

    # Add the water_level argument with the updated description
    parser.add_argument(
        "-w",
        "--water_level",
        required=True,
        type=str,
        help="water_level must be one of: 'skip', 'download', "
        "or a path indicating the location of the water level data",
    )
    parser.add_argument(
        "-y", "--years", nargs="+", help="years for which data is downloaded"
    )
    parser.add_argument(
        "-e",
        "--era5",
        required=True,
        type=str,
        help="era5 must be one of: 'skip', 'download', "
        "or a path indicating the location of the era5 data",
    )

    parser.add_argument(
        "-c",
        "--climate_variables",
        nargs="+",
        required=True,
        type=str,
        help="one ore multiple short names of climate variables",
    )
    # Parse the arguments
    args = parser.parse_args()

    def check_if_path(arg):
        """checks the value of water_level and era5"""
        if arg not in ["skip", "download"]:
            # Try to create a Path object and check if it exists
            path = Path(arg)
            if not path.exists():
                raise FileNotFoundError(f"The path {path} does not exist.")
            return path
        return arg

    main(
        water_level=check_if_path(args.water_level),
        era5=check_if_path(args.era5),
        years=args.years,
        climate_variables=args.climate_variables,
    )
