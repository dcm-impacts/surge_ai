import argparse
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np


def plot_loss_with_trend(filename):
    epochs = []
    losses = []

    # Read data from file
    with open(filename, "r") as file:
        for line in file:
            parts = line.split(",")
            epoch_info = parts[0].split(" ")[1]
            epoch = int(epoch_info.split("/")[0])
            loss = float(parts[1].split(": ")[1])
            epochs.append(epoch)
            losses.append(loss)

    # Convert lists to numpy arrays for plotting and analysis
    epochs = np.array(epochs)
    losses = np.array(losses)

    # Fitting a linear regression line to the data
    coefficients = np.polyfit(epochs, losses, 1)
    trend_line = np.polyval(coefficients, epochs)

    # Plotting
    plt.plot(epochs, losses, label=filename.stem)
    plt.plot(epochs, trend_line, "r--", label="Trend Line")


# You would call the function like this, assuming the data is saved in 'loss_data.txt'
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "filenames",
        type=str,
        nargs="+",
        help="Pass the filename containing the loss data",
    )
    args = parser.parse_args()
    plt.figure(figsize=(10, 6))
    for filename in args.filenames:
        filename = Path(filename)
        plot_loss_with_trend(filename)

    plt.title("Loss vs. Epochs with Trend Line")
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.legend()
    plt.grid(True)

    plt.savefig(f"{filename.stem}.png")
    plt.show()
