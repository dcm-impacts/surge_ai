import argparse
import pdb
import warnings
from itertools import combinations
from math import ceil

import cartopy.crs as ccrs  # type: ignore
import cartopy.feature as cfeature  # type: ignore
import lightning as L
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd
import torch
from matplotlib import transforms
from zuko.flows import MAF  # type: ignore

from surge_ai.data.dataset import GluontsDataset
from surge_ai.lightning_flow import LitFlow


def generate_surge_distribution(flow, predictor, n_stations, n_samples=10000):
    """
    Generate a predicted distribution using a flow model and predictor data in parallel on GPU.

    Parameters:
    -----------
    flow : torch.nn.Module or callable
        The flow model used for generating distributions. Must have a `.sample()` or `.transform.inv()` method.
    predictor : pd.DataFrame or np.ndarray
        Predictor data used as input to the flow model.
    n_stations : int
        Number of stations for each sample.
    n_samples : int, optional
        Number of samples to generate for each timestep (default is 10,000).
    z : torch.Tensor, optional
        Pre-generated random noise vector for sampling. If None, random noise of size `(len(predictor), n_samples, n_stations)`
        will be generated. Default is None.

    Returns:
    --------
    predicted_dist : torch.Tensor
        Tensor containing generated distributions for each timestep, stored on the CPU.
    """

    # Ensure no gradient tracking for inference
    with torch.no_grad():
        # Convert predictor to tensor on the specified device
        predictor_tensor = torch.tensor(predictor, dtype=torch.float32, device="cpu")

        # Apply the model and inverse transform in batches to reduce memory usage
        # Here we assume that `flow` is a callable that processes each timestep in a batch-friendly way
        predicted_dist = flow(predictor_tensor).sample((n_samples,))

        # Move predictions to CPU and detach them from GPU computation graph
        return predicted_dist.cpu().numpy()


def parse_arguments(args=None):
    parser = argparse.ArgumentParser(
        description="Script to train a model with specified configurations."
    )

    # Add "checkpoint" argument to load a stored model
    parser.add_argument(
        "--checkpoint",
        type=str,
        default=None,
        help="Path to a checkpoint file to load a stored model (default: None)",
    )

    # Add "n_stations" argument
    parser.add_argument(
        "--n_stations",
        type=int,
        default=16,
        help="Number of stations to use for training.",
    )

    parser.add_argument(
        "--dataset",
        type=str,
        default="storm_surge_16stationsNL",
        help="name of the dataset from the dataset repository",
    )
    parser.add_argument(
        "--regenerate",
        action="store_true",
        help="regenerate the dataset if set to true",
    )

    # Add "max_epochs" argument
    parser.add_argument(
        "--max_epochs",
        type=int,
        default=40,
        help="Maximum number of epochs for training.",
    )

    return parser.parse_args(args)


def plot_percentile_on_map(percentile, station_lats, station_lons):
    # Set up map boundaries
    lat_min = min(station_lats) - 5
    lon_min = min(station_lons) - 5
    lat_max = max(station_lats) + 5
    lon_max = max(station_lons) + 5

    # Create the map figure
    fig = plt.figure(figsize=(20, 20))
    ax_map = fig.add_subplot(111, projection=ccrs.PlateCarree())
    ax_map.set_extent([lon_min, lon_max, lat_min, lat_max])
    ax_map.add_feature(cfeature.COASTLINE)
    ax_map.add_feature(cfeature.BORDERS)
    ax_map.add_feature(cfeature.LAND, color="lightgray")

    # Plot the stations on the map for reference
    ax_map.scatter(
        station_lons,
        station_lats,
        c=percentile,
        s=50,
        transform=ccrs.PlateCarree(),
        zorder=5,
    )

    plt.savefig(
        "figures/percentile_map.jpg", format="jpg", dpi=300, bbox_inches="tight"
    )
    plt.close()


def plot_surge_dist(n_stations, test_dataset, percentiles, surge_dist):
    date = test_dataset[:][2].numpy()
    date_indices = np.array([np.datetime64(int(d), "ns") for d in date])
    selected_target_data = test_dataset[:][0]

    nrows = ceil(n_stations / 4)
    if nrows == 1:
        ncols = n_stations
    else:
        ncols = 4

    fig, axes = plt.subplots(nrows, ncols, figsize=(15, nrows * 4))
    if n_stations == 1:
        axes = [axes]
    else:
        axes = axes.flatten()  # Flatten to easily iterate over axes

    start_date = np.datetime64("2014-07-03")
    end_date = np.datetime64("2014-07-13")
    idx_sel = (date_indices >= start_date) & (date_indices <= end_date)
    x_sel = date_indices[idx_sel]

    for i in range(n_stations):
        ax = axes[i]
        ax.plot(x_sel, percentiles[50][idx_sel, i], label="Median", color="C0")

        # Fill 90% Prediction Interval
        ax.fill_between(
            x_sel,
            percentiles[5][idx_sel, i],
            percentiles[95][idx_sel, i],
            color="gray",
            alpha=0.3,
            label="90% Prediction Interval",
        )

        # Fill 50% Prediction Interval
        ax.fill_between(
            x_sel,
            percentiles[25][idx_sel, i],
            percentiles[75][idx_sel, i],
            color="orange",
            alpha=0.3,
            label="50% Prediction Interval",
        )

        ax.plot(x_sel, surge_dist[0, idx_sel, i], c="C2", label="Sample")
        ax.plot(x_sel, selected_target_data[idx_sel, i], c="black", label="Target")

        ax.set_title(f"Subplot {i+1}")
        ax.set_xlabel("X-axis label")
        ax.set_ylabel("Y-axis label")
        ax.set_ylim(
            [percentiles[5][idx_sel, :].min(), percentiles[95][idx_sel, :].max()]
        )
        ax.tick_params(axis="x", rotation=30)

    # Optionally, add a single legend for all subplots
    handles, labels = ax.get_legend_handles_labels()
    fig.legend(handles, labels, loc="upper center", ncol=4)
    plt.savefig(
        "figures/surge_distribution.jpg", format="jpg", dpi=300, bbox_inches="tight"
    )
    plt.close()


def plot_correlation_matrix(correlation_matrix, label):
    """
    Calculates and plots the correlation matrix between stations in a single array.
    """
    plt.figure(figsize=(10, 8))
    plt.imshow(correlation_matrix, cmap="coolwarm", vmin=-1, vmax=1)
    plt.colorbar(label="Correlation Coefficient")
    plt.title(f"Station Correlation Matrix for {label}")
    plt.xlabel("Stations")
    plt.ylabel("Stations")
    plt.savefig(
        f"figures/correlation_matrix_{label}.jpg",
        format="jpg",
        dpi=300,
        bbox_inches="tight",
    )
    plt.close()


def plot_spatial_correlation_network(
    lat,
    lon,
    correlation_matrix,
    label,
    threshold=0.5,
    positive_color="red",
    negative_color="blue",
):
    """
    Plots a spatial correlation network with positive and negative correlations differentiated.

    Parameters:
    - lat (np.ndarray): 1D numpy array of latitude values for each station.
    - lon (np.ndarray): 1D numpy array of longitude values for each station.
    - correlation_matrix (np.ndarray): Square numpy array with correlation values between stations.
    - threshold (float): Minimum absolute correlation value to include an edge between stations.
    - positive_color (str): Color for edges with positive correlation.
    - negative_color (str): Color for edges with negative correlation.
    """

    # Create a graph
    G = nx.Graph()

    # Add nodes with geographic position
    for i in range(len(lat)):
        G.add_node(i, pos=(lon[i], lat[i]))

    # Add edges with weights from the correlation matrix
    for i, j in combinations(range(len(lat)), 2):
        corr_value = correlation_matrix[i, j]
        if abs(corr_value) >= threshold:
            G.add_edge(
                i,
                j,
                weight=abs(corr_value),
                corr_type="positive" if corr_value > 0 else "negative",
            )

    # Extract node positions for plotting
    pos = nx.get_node_attributes(G, "pos")

    # Set up Cartopy map with PlateCarree projection
    fig, ax = plt.subplots(
        figsize=(10, 6), subplot_kw={"projection": ccrs.PlateCarree()}
    )
    ax.set_global()  # Set global extent for world map
    ax.coastlines()  # Add coastlines for context
    ax.add_feature(cfeature.BORDERS, linestyle=":", alpha=0.5)
    ax.set_xlim([min(lon) - 1, max(lon) + 1])
    ax.set_ylim([min(lat) - 1, max(lat) + 1])

    # Convert positions to the Cartopy projection manually for matplotlib plotting
    # Separate edges by correlation type
    edges_pos = [
        (u, v) for u, v, d in G.edges(data=True) if d["corr_type"] == "positive"
    ]
    edges_neg = [
        (u, v) for u, v, d in G.edges(data=True) if d["corr_type"] == "negative"
    ]

    # Get weights for edge thickness
    weights_pos = [4 * (G[u][v]["weight"] - threshold) for u, v in edges_pos]
    weights_neg = [4 * (G[u][v]["weight"] - threshold) for u, v in edges_neg]

    # Draw positive correlation edges
    for (u, v), weight in zip(edges_pos, weights_pos):
        x_coords = [pos[u][0], pos[v][0]]
        y_coords = [pos[u][1], pos[v][1]]
        ax.plot(
            x_coords,
            y_coords,
            color=positive_color,
            linewidth=weight,
            alpha=0.6,
            transform=ccrs.PlateCarree(),
        )

    # Draw negative correlation edges
    for (u, v), weight in zip(edges_neg, weights_neg):
        x_coords = [pos[u][0], pos[v][0]]
        y_coords = [pos[u][1], pos[v][1]]
        ax.plot(
            x_coords,
            y_coords,
            color=negative_color,
            linewidth=weight,
            alpha=0.6,
            transform=ccrs.PlateCarree(),
        )

    for node, (x, y) in pos.items():
        ax.plot(
            x, y, marker="o", color="blue", markersize=2, transform=ccrs.PlateCarree()
        )

    plt.legend(["Positive Correlation", "Negative Correlation"], loc="upper right")

    plt.title("Network Visualization of Spatial Correlations")
    plt.xlabel("Longitude")
    plt.ylabel("Latitude")
    plt.savefig(
        f"figures/network_visualization_spatial_correlations_{label}.jpg",
        format="jpg",
        dpi=300,
        bbox_inches="tight",
    )
    plt.close()


def plot_all_stations_on_map(lat, lon, name="all_stations_on_map", idxs=None):
    fig, ax = plt.subplots(
        figsize=(10, 6), subplot_kw={"projection": ccrs.PlateCarree()}
    )
    ax.set_global()  # Set global extent for world map
    ax.coastlines()  # Add coastlines for context
    ax.add_feature(cfeature.BORDERS, linestyle=":", alpha=0.5)
    ax.set_xlim([min(lon) - 1, max(lon) + 1])
    ax.set_ylim([min(lat) - 1, max(lat) + 1])

    if idxs is None:
        idxs = [i for i in range(len(lat))]

    for i in idxs:
        ax.plot(
            lon[i],
            lat[i],
            marker="o",
            color="blue",
            markersize=2,
            transform=ccrs.PlateCarree(),
        )
        ax.text(
            lon[i],
            lat[i],
            str(i + 1),
            color="black",
            fontsize=8,
            ha="right",
            va="bottom",
            transform=ccrs.PlateCarree(),
        )

    plt.title("All stations on a map")
    plt.xlabel("Longitude")
    plt.ylabel("Latitude")
    plt.savefig(
        f"figures/{name}.jpg",
        format="jpg",
        dpi=300,
        bbox_inches="tight",
    )
    plt.close()


def main(conf):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    gluonts_dataset = GluontsDataset(conf.n_stations, conf.dataset, conf.regenerate)
    datasets = gluonts_dataset.datasets
    dataloaders = gluonts_dataset.dataloaders

    _flow = MAF(
        features=conf.n_stations,
        context=len(datasets["train"][0][1]),
        transforms=2,
        hidden_features=(64, 64),
    )
    if conf.checkpoint is None:
        print(f"loading model from {conf.checkpoint=}")
        flow = LitFlow(_flow)
        trainer = L.Trainer(limit_train_batches=500, max_epochs=conf.max_epochs)
        trainer.fit(model=flow, train_dataloaders=dataloaders["train"])
    else:
        flow = LitFlow.load_from_checkpoint(conf.checkpoint, flow=_flow)

    flow.eval()
    selected_predictor_data = datasets["test"][:][1]
    surge_dist = generate_surge_distribution(
        flow.flow.to("cpu"),
        selected_predictor_data,
        conf.n_stations,
        n_samples=200,
    )

    percentiles = {
        percentile: np.nanpercentile(surge_dist, percentile, axis=0)
        for percentile in [5, 25, 50, 75, 95]
    }
    plot_surge_dist(
        n_stations=conf.n_stations,
        test_dataset=datasets["test"],
        percentiles=percentiles,
        surge_dist=surge_dist,
    )
    sample_estimate_err = {
        "data": np.corrcoef(
            surge_dist[0, :, :] - surge_dist.mean(axis=0), rowvar=False
        ),
        "name": "sample_estimate-mean_estimate",
    }
    obs_err = {
        "data": np.corrcoef(
            datasets["test"][:][0].numpy() - surge_dist.mean(axis=0), rowvar=False
        ),
        "name": "obs-mean_estimate",
    }

    # Extract data from the dictionary

    plot_correlation_matrix(obs_err["data"], obs_err["name"])
    plot_correlation_matrix(sample_estimate_err["data"], obs_err["name"])

    plot_spatial_correlation_network(
        lat=gluonts_dataset.lat,
        lon=gluonts_dataset.lon,
        correlation_matrix=obs_err["data"],
        label=obs_err["name"],
    )

    plot_spatial_correlation_network(
        lat=gluonts_dataset.lat,
        lon=gluonts_dataset.lon,
        correlation_matrix=obs_err["data"],
        label=f"{obs_err['name']}_thr_0.8",
        threshold=0.8,
    )

    plot_spatial_correlation_network(
        lat=gluonts_dataset.lat,
        lon=gluonts_dataset.lon,
        correlation_matrix=sample_estimate_err["data"],
        label=sample_estimate_err["name"],
    )

    plot_spatial_correlation_network(
        lat=gluonts_dataset.lat,
        lon=gluonts_dataset.lon,
        correlation_matrix=sample_estimate_err["data"],
        label=f"{sample_estimate_err['name']}_thr_0.8",
        threshold=0.8,
    )

    plot_all_stations_on_map(lat=gluonts_dataset.lat, lon=gluonts_dataset.lon)
    plot_all_stations_on_map(
        lat=gluonts_dataset.lat,
        lon=gluonts_dataset.lon,
        name="sel_stations_map",
        idxs=[82, 35, 30, 73, 55, 17],
    )


if __name__ == "__main__":
    warnings.filterwarnings(
        "ignore", category=FutureWarning, message=".*'H' is deprecated.*"
    )
    conf = parse_arguments()
    main(conf)
