import lightning as L
from torch import optim


class LitFlow(L.LightningModule):
    def __init__(self, flow):
        super().__init__()
        self.flow = flow

    def training_step(self, batch, batch_idx):
        x, label, _ = batch
        x = x.float()
        label = label.float()
        c = label
        loss = -self.flow(c).log_prob(x).mean()
        self.log("train_loss", loss)
        return loss

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=1e-3)
        return optimizer
