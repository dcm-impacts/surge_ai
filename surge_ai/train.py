import time


def train_model(model, train_loader, criterion, optimizer, num_epochs):
    model.train()
    for epoch in range(num_epochs):
        start_time = time.time()
        loss = None
        for inputs, targets in train_loader:
            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, targets)
            loss.backward()
            optimizer.step()

        if loss is None:
            raise ValueError("train_loader is empty")

        duration = time.time() - start_time
        print(
            f"Epoch {epoch+1}/{num_epochs}, Loss: {loss.item():.4f}, Time: {duration:.2f}"
        )
