import torch
import torch.nn as nn


class SurgePredictorLSTM(nn.Module):
    def __init__(
        self, input_size, hidden_size, num_layers, prediction_length, sequence_length
    ):
        super(SurgePredictorLSTM, self).__init__()
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers, batch_first=True)
        self.fc = nn.Linear(
            hidden_size * sequence_length, prediction_length
        )  # predicting 24 hourly surge levels
        self.num_layers = num_layers
        self.input_size = input_size
        self.hidden_size = hidden_size

    def forward(self, x):
        # Forward propagate LSTM
        h0 = torch.randn(self.num_layers, x.shape[0], self.hidden_size)
        c0 = torch.randn(self.num_layers, x.shape[0], self.hidden_size)
        out, _ = self.lstm(x, (h0, c0))
        # Decode the hidden state of the last time step
        # out = self.fc(out)
        return self.fc(out.reshape(x.shape[0], -1))
