# Download gtsm data from the copernicus Climate Data Service

import tarfile
from pathlib import Path

import cdsapi


def download_codec(years):
    c = cdsapi.Client()

    c.retrieve(
        "sis-water-level-change-timeseries-cmip6",
        {
            "variable": "storm_surge_residual",
            "experiment": "reanalysis",
            "temporal_aggregation": "hourly",
            "year": years,
            "month": [
                "01",
                "02",
                "03",
                "04",
                "05",
                "06",
                "07",
                "08",
                "09",
                "10",
                "11",
                "12",
            ],
            "format": "tgz",
        },
        "download.tar.gz",
    )

    target_dir = Path("data")
    target_dir.mkdir(exist_ok=True)

    tar = tarfile.open("download.tar.gz")
    tar.extractall(path=target_dir)
    tar.close()
