# Generate a storm surge dataset.
import json
import os
import pdb
import shutil
from functools import lru_cache
from pathlib import Path
from typing import NamedTuple, Optional

import numpy as np
import pandas as pd   # type: ignore
import xarray as xr
from gluonts.dataset import DatasetWriter   # type: ignore
from gluonts.dataset.common import Dataset, MetaData   # type: ignore
from gluonts.dataset.repository._util import metadata   # type: ignore
from typing_extensions import Optional


class TrainValDatasets(NamedTuple):
    """
    A dataset containing three subsets: one for training, one for validation, and
    one for testing purposes, along with metadata.
    """

    metadata: MetaData
    train: Dataset
    val: Optional[Dataset] = None
    test: Optional[Dataset] = None

    def save(
        self,
        path_str: str,
        writer: DatasetWriter,
        overwrite=False,
    ) -> None:
        """
        Saves a TrainValDatasets object to a JSON Lines file.

        Parameters
        ----------
        path_str
            Where to save the dataset.
        overwrite
            Whether to delete the previous version in this folder.
        """
        path = Path(path_str)

        if overwrite:
            shutil.rmtree(path, ignore_errors=True)

        path.mkdir(parents=True)
        with open(path / "metadata.json", "w") as out_file:
            json.dump(self.metadata.dict(), out_file, ensure_ascii=False, indent=4)

        train = path / "train"
        train.mkdir(parents=True)
        writer.write_to_folder(self.train, train)

        if self.val is not None:
            val = path / "val"
            val.mkdir(parents=True)
            writer.write_to_folder(self.val, val)

        if self.test is not None:
            test = path / "test"
            test.mkdir(parents=True)
            writer.write_to_folder(self.test, test)


def transform_longitude(longitudes):
    if isinstance(longitudes, xr.DataArray):
        return xr.where(longitudes < 0, longitudes + 360, longitudes)
    else:
        # If it's a single value, apply the transformation directly
        return longitudes + 360 if longitudes < 0 else longitudes


def generate_reanalysis_surge_dataset_xcov(
    dataset_path: Path,
    train_data_path: Path,
    val_data_path: Path,
    test_data_path: Path,
    external_train_data_path: Path,
    external_test_data_path: Path,
    external_val_data_path: Path,
    dataset_writer: DatasetWriter,
    lat_min: float,
    lat_max: float,
    lon_min: float,
    lon_max: float,
    prediction_length: int = 24,
    n_stations: Optional[int] = None,
    pandas_freq: str = "H",
):
    def get_station_indices_within_bounds(
        file_path, lat_min, lat_max, lon_min, lon_max, n=None
    ):
        with xr.open_dataset(file_path) as ds:
            stations_in_bounds = ds.where(
                (ds.station_y_coordinate >= lat_min)
                & (ds.station_y_coordinate <= lat_max)
                & (ds.station_x_coordinate >= lon_min)
                & (ds.station_x_coordinate <= lon_max),
                drop=True,
            )
            station_indices = stations_in_bounds.stations.values
            if n is not None and n < len(station_indices):
                indices = np.linspace(0, len(station_indices) - 1, n, dtype=int)
                sampled_indices = station_indices[indices]
            else:
                sampled_indices = station_indices
            return sampled_indices

    def load_and_concat_nc_files(directory, stations, file_pattern="*.nc"):
        directory = Path(directory)
        nc_files = sorted(directory.glob(file_pattern))

        def load_file(file):
            return xr.load_dataset(file).sel(stations=stations)

        datasets = []
        for file in nc_files:
            datasets.append(load_file(file))
        combined_dataset = xr.concat(datasets, dim="time")
        return combined_dataset

    def load_and_concat_external_covariates(directory, surge_data, file_pattern):
        directory = Path(directory)
        nc_files = sorted(directory.glob(file_pattern))

        def load_file(file):
            with xr.open_dataset(file) as ds:
                ds_sel = ds.sel(
                    latitude=surge_data.station_y_coordinate,
                    longitude=transform_longitude(surge_data.station_x_coordinate),
                    method="nearest",
                )
            return ds_sel

        datasets = []
        for file in nc_files:
            datasets.append(load_file(file))
        combined_dataset = xr.concat(datasets, dim="time")
        return combined_dataset

    # Get station indices within the geographical bounds
    station_indices = get_station_indices_within_bounds(
        train_data_path / Path("reanalysis_surge_hourly_1979_01_v1.nc"),
        lat_min,
        lat_max,
        lon_min,
        lon_max,
        n_stations,
    )

    # Load and concatenate datasets for train and test
    train_surge = load_and_concat_nc_files(train_data_path, station_indices)
    test_surge = load_and_concat_nc_files(test_data_path, station_indices)
    val_surge = load_and_concat_nc_files(val_data_path, station_indices)
    train_ps = load_and_concat_external_covariates(
        directory=external_train_data_path / Path("ps"),
        surge_data=train_surge,
        file_pattern="ps_1hr_ECMWF-ERA5_observation_*_rechunked.nc",
    )
    test_ps = load_and_concat_external_covariates(
        directory=external_test_data_path / Path("ps"),
        surge_data=test_surge,
        file_pattern="ps_1hr_ECMWF-ERA5_observation_*_rechunked.nc",
    )
    val_ps = load_and_concat_external_covariates(
        directory=external_val_data_path / Path("ps"),
        surge_data=val_surge,
        file_pattern="ps_1hr_ECMWF-ERA5_observation_*_rechunked.nc",
    )

    train_ds: list[dict] = []
    val_ds: list[dict] = []
    test_ds: list[dict] = []
    for station in station_indices:
        for mode in "train", "val", "test":
            time_series = {
                "start": pd.to_datetime(locals()[f"{mode}_surge"].time.values[0]),
                # "station_id": station,
                "target": locals()[f"{mode}_surge"]
                .sel(stations=station)
                .surge.to_numpy(),
                "feat_dynamic_real": locals()[f"{mode}_ps"]
                .sel(stations=station)
                .ps.to_numpy()[None, :],
                "lat": locals()[f"{mode}_surge"]
                .sel(stations=station)
                .station_y_coordinate.item(),
                "lon": locals()[f"{mode}_surge"]
                .sel(stations=station)
                .station_x_coordinate.item(),
            }
            locals()[f"{mode}_ds"].append(time_series.copy())

    # Define metadata
    meta = MetaData(
        **metadata(
            cardinality=[train_surge.surge.shape[1]],
            freq=pandas_freq,
            prediction_length=prediction_length,
        )
    )

    # Save datasets
    dataset = TrainValDatasets(metadata=meta, train=train_ds, test=test_ds, val=val_ds)
    dataset.save(path_str=str(dataset_path), writer=dataset_writer, overwrite=True)

    print(f"Dataset saved at {dataset_path}")
