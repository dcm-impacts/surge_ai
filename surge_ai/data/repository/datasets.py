from functools import partial
from pathlib import Path

from gluonts.dataset.repository.datasets import (dataset_recipes,
                                                 get_download_path)

from ._storm_surge_xcov import generate_reanalysis_surge_dataset_xcov

dataset_recipes["storm_surge_80stations_south_japan"] = partial(
    generate_reanalysis_surge_dataset_xcov,
    train_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/train/reanalysis_surge/"
    ),
    val_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/val/reanalysis_surge/"
    ),
    test_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/test/reanalysis_surge/"
    ),
    external_train_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/train/ERA5_2deg/"
    ),
    external_val_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/val/ERA5_2deg/"
    ),
    external_test_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/test/ERA5_2deg/"
    ),
    lat_min=30.719136,
    lat_max=33.913661,
    lon_min=128.934206,
    lon_max=132.168181,
    n_stations=80,
    pandas_freq="H",
    prediction_length=24,
)

dataset_recipes["storm_surge_100stations_japan"] = partial(
    generate_reanalysis_surge_dataset_xcov,
    train_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/train/reanalysis_surge/"
    ),
    val_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/val/reanalysis_surge/"
    ),
    test_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/test/reanalysis_surge/"
    ),
    external_train_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/train/ERA5_2deg/"
    ),
    external_val_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/val/ERA5_2deg/"
    ),
    external_test_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/test/ERA5_2deg/"
    ),
    lat_min=30.719136,
    lat_max=46.067225,
    lon_min=128.934206,
    lon_max=148.063286,
    n_stations=100,
    pandas_freq="H",
    prediction_length=24,
)


dataset_recipes["storm_surge_16stationsNL_1ps"] = partial(
    generate_reanalysis_surge_dataset_xcov,
    train_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/train/reanalysis_surge/"
    ),
    val_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/val/reanalysis_surge/"
    ),
    test_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/test/reanalysis_surge/"
    ),
    external_train_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/train/ERA5_2deg/"
    ),
    external_val_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/val/ERA5_2deg/"
    ),
    external_test_data_path=Path(
        "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/test/ERA5_2deg/"
    ),
    lat_min=50.7,
    lat_max=53.5,
    lon_min=3.4,
    lon_max=7.1,
    n_stations=16,
    pandas_freq="H",
    prediction_length=24,
)
