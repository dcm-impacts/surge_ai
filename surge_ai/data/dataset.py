from pathlib import Path

import numpy as np
import pandas as pd
import torch
import xarray as xr
from gluonts.dataset.repository.datasets import get_dataset
from gluonts.dataset.util import to_pandas
from torch.utils.data import DataLoader, Dataset, TensorDataset

from surge_ai.data.repository.datasets import dataset_recipes


def reshape_data(x: np.ndarray, y: np.ndarray, seq_length: int):
    """Reshape data into LSTM many-to-one input samples

    Parameters
    ----------
    x : np.ndarray
        Input features of shape [num_samples, num_features]
    y : np.ndarray
        Output feature of shape [num_samples, 1]
    seq_length : int
        Length of the requested input sequences.

    Returns
    -------
    x_new: np.ndarray
        Reshaped input features of shape [num_samples*, seq_length, num_features], where
        num_samples* is equal to num_samples - seq_length + 1, due to the need of a warm start at
        the beginning
    y_new: np.ndarray
        The target value for each sample in x_new
    """
    num_samples, num_features = x.shape

    x_new = np.zeros((num_samples - seq_length + 1, seq_length, num_features))
    y_new = np.zeros((num_samples - seq_length + 1, 1))

    for i in range(0, x_new.shape[0]):
        x_new[i, :, :num_features] = x[i : i + seq_length, :]
        y_new[i, :] = y[i + seq_length - 1, 0]

    return x_new, y_new


def reshape_data_with_surge_as_feature(x: np.ndarray, y: np.ndarray, seq_length: int):
    """Reshape data into LSTM many-to-one input samples

    Parameters
    ----------
    x : np.ndarray
        Input features of shape [num_samples, num_features]
    y : np.ndarray
        Output feature of shape [num_samples, 1]
    seq_length : int
        Length of the requested input sequences.

    Returns
    -------
    x_new: np.ndarray
        Reshaped input features of shape [num_samples*, seq_length, num_features], where
        num_samples* is equal to num_samples - seq_length, due to the need of a warm start at
        the beginning
    y_new: np.ndarray
        The target value for each sample in x_new
    """
    num_samples, num_features = x.shape

    x_new = np.zeros((num_samples - seq_length, seq_length, num_features))
    y_new = np.zeros((num_samples - seq_length, 1))

    for i in range(0, x_new.shape[0]):
        x_new[i, :, :num_features] = x[i : i + seq_length, :]
        y_new[i, :] = y[i + seq_length, 0]

    return x_new, y_new


def transform_longitude(longitude):
    if longitude < 0:
        return longitude + 360
    else:
        return longitude


class CoastalWaterLevelDataset(Dataset):
    def __init__(
        self,
        surge_dir,
        ps_dir,
        uas_dir,
        vas_dir,
        sequence_length=72,
        is_surge_feature=False,
        station_idx=0,
    ):
        """
        Args:
            surge_dir: Directory with the surge data NetCDF files.
            ps_dir: Directory with the ps data NetCDF files.
            uas_dir: Directory with the uas data NetCDF files.
            vas_dir: Directory with the vas data NetCDF files.
            sequence_length: Length of the input sequence in hours.
            prediction_length: Length of the output sequence (prediction) in hours.
        """
        self.sequence_length = sequence_length
        self.is_surge_feature = is_surge_feature
        self.station_idx = station_idx
        self.x, self.y = self._load_data(surge_dir, ps_dir, uas_dir, vas_dir)

    def __len__(self):
        # The length is the number of possible sequences we can return
        return self.x.shape[0]

    def __getitem__(self, idx):
        return self.x[idx], self.y[idx]

    def _load_data(self, surge_dir, ps_dir, uas_dir, vas_dir):
        """load data from netcdf files"""

        surge_data = xr.open_mfdataset(
            f"{surge_dir}/reanalysis_surge_hourly_*.nc", combine="by_coords"
        ).isel(stations=self.station_idx)
        ps_data = xr.open_mfdataset(
            f"{ps_dir}/ps_1hr_ECMWF-ERA5_observation_*_rechunked.nc",
            combine="by_coords",
        ).sel(
            latitude=surge_data.station_y_coordinate,
            longitude=transform_longitude(surge_data.station_x_coordinate),
            method="nearest",
        )
        uas_data = xr.open_mfdataset(
            f"{uas_dir}/uas_1hr_ECMWF-ERA5_observation_*_rechunked.nc",
            combine="by_coords",
        ).sel(
            latitude=surge_data.station_y_coordinate,
            longitude=transform_longitude(surge_data.station_x_coordinate),
            method="nearest",
        )
        vas_data = xr.open_mfdataset(
            f"{vas_dir}/vas_1hr_ECMWF-ERA5_observation_*_rechunked.nc",
            combine="by_coords",
        ).sel(
            latitude=surge_data.station_y_coordinate,
            longitude=transform_longitude(surge_data.station_x_coordinate),
            method="nearest",
        )
        # use all meteorological variables as input

        if self.is_surge_feature:
            x = np.array(
                [
                    surge_data.surge.values,
                    ps_data.ps.values,
                    uas_data.uas.values,
                    vas_data.vas.values,
                ]
            ).T
        else:
            x = np.array(
                [
                    ps_data.ps.values,
                    uas_data.uas.values,
                    vas_data.vas.values,
                ]
            ).T

        y_flat = surge_data.surge.values
        not_nan = np.where(~np.isnan(y_flat))[0]
        nan_indices = np.where(np.isnan(y_flat))[0]

        if len(nan_indices) == len(y_flat):
            raise ValueError("All y values are nan")
        elif len(nan_indices) >= len(y_flat) / 3:
            raise ValueError("More than a third of the y values are nan")
        elif len(nan_indices) > 0:
            print(
                f"Warning: found {len(nan_indices)} nan values. The values were linearly interpolated"
            )
            y_flat[nan_indices] = np.interp(nan_indices, not_nan, y_flat[not_nan])
        y = np.array([y_flat]).T

        self.x_mean = x.mean(axis=0)
        self.x_std = x.std(axis=0)
        self.y_mean = y.mean(axis=0)
        self.y_std = y.std(axis=0)
        # normalize
        x = (x - self.x_mean) / self.x_std
        y = (y - self.y_mean) / self.y_std
        if x.shape[0] != y.shape[0]:
            raise ValueError(
                f"The length of the input and output time series do not match. "
                f"The input time series has length {x.shape[0]}."
                f"The output time series has length {y.shape[0]}."
                f"Please check if all required files are in the data directory."
            )
        if self.is_surge_feature:
            x, y = reshape_data_with_surge_as_feature(x, y, self.sequence_length)
        else:
            x, y = reshape_data(x, y, self.sequence_length)
        x = torch.from_numpy(x.astype(np.float32))
        y = torch.from_numpy(y.astype(np.float32))
        return x, y


class GluontsDataset:
    def __init__(self, n_stations, dataset_name, regenerate):
        self.n = 30  # Number of previous time steps to use as predictors
        self.n_stations = n_stations
        dataset = get_dataset(
            dataset_name,
            regenerate=regenerate,
            path=Path(
                "/p/projects/isimip/isimip/sitreu/projects/surge_ai/data/repository"
            ),
        )
        data_list_train = list(dataset.train)[: self.n_stations]
        # data_list_val = list(dataset.val)[:n_stations]
        data_list_test = list(dataset.test)[: self.n_stations]
        self.lat = [data["lat"] for data in data_list_train]
        self.lon = [data["lon"] for data in data_list_train]

        train_dataset, train_loader, train_metadata = self.process_train_data(
            data_list_train
        )
        test_dataset, test_loader = self.process_test_data(
            data_list_test, train_metadata
        )
        # val_dataset, val_loader = self.process_test_data(data_list_val, train_metadata)
        self.datasets = {
            "train": train_dataset,
            # "val": val_dataset,
            "test": test_dataset,
        }
        self.dataloaders = {
            "train": train_loader,
            # "val": val_loader,
            "test": test_loader,
        }
        lat_array = np.array([d["lat"] for d in data_list_train])
        lon_array = np.array([d["lon"] for d in data_list_train])
        self.lat = lat_array
        self.lon = lon_array

    def process_train_data(self, data_list):
        # Create surge and ps DataFrames
        surge_df = pd.DataFrame(
            {i: to_pandas(data_list[i]) for i in range(self.n_stations)}
        ).interpolate()
        feature_list = []
        for i in range(len(data_list)):
            feature = data_list[i].copy()
            feature["target"] = data_list[i]["feat_dynamic_real"][0, :]
            feature_list.append(feature)

        ps_df = pd.DataFrame(
            {i: to_pandas(feature_list[i]) for i in range(self.n_stations)}
        ).interpolate()

        surge_df.index = surge_df.index.to_timestamp()
        ps_df.index = ps_df.index.to_timestamp()

        ps_tensor_list = []
        surge_tensor_list = []
        date_tensor_list = []

        # Iterate and process with resampling and shifting
        for i in range(24):
            # Apply resampling to 24h instead of rolling window
            surge_df_resampled = surge_df.resample("24h").max()
            ps_df_resampled = ps_df.resample("24h").mean()

            # Drop NaN values from both DataFrames to ensure valid data only
            surge_df_resampled = surge_df_resampled.dropna()
            ps_df_resampled = ps_df_resampled.dropna()

            # Assert that the indices are the same for both DataFrames after dropping NaNs
            assert surge_df_resampled.index.equals(
                ps_df_resampled.index
            ), "Indices mismatch between surge_df and ps_df after resampling and dropping NaNs."

            # Create shifted data and PyTorch tensors
            _ps_tensor, _surge_tensor, _date_tensor = (
                self.create_shifted_data_and_tensors(
                    ps_df_resampled,
                    surge_df_resampled,
                )
            )

            # Append tensors to the respective lists
            ps_tensor_list.append(_ps_tensor)
            surge_tensor_list.append(_surge_tensor)
            date_tensor_list.append(_date_tensor)

            # Shift surge_df and ps_df one hour to the right for the next iteration
            surge_df = surge_df.shift(periods=-1, freq="h")
            ps_df = ps_df.shift(periods=-1, freq="h")

        # Concatenate the list of predictor, predictand, and date tensors
        ps_tensor = torch.cat(ps_tensor_list, dim=0)
        ps_tensor, ps_mean, ps_std = normalize(ps_tensor)
        surge_tensor = torch.cat(surge_tensor_list, dim=0)
        surge_tensor, surge_mean, surge_std = normalize(surge_tensor)
        date_tensor = torch.cat(date_tensor_list, dim=0)

        metadata = {
            "ps_mean": ps_mean,
            "ps_std": ps_std,
            "surge_mean": surge_mean,
            "surge_std": surge_std,
        }

        # Create TensorDataset and DataLoader
        dataset = TensorDataset(surge_tensor, ps_tensor, date_tensor)
        loader = DataLoader(dataset, batch_size=512, shuffle=False)

        return dataset, loader, metadata

    def process_test_data(self, data_list, train_metadata):
        # Create surge and ps DataFrames
        surge_df = pd.DataFrame(
            {i: to_pandas(data_list[i]) for i in range(self.n_stations)}
        ).interpolate()
        feature_list = []
        for i in range(len(data_list)):
            feature = data_list[i].copy()
            feature["target"] = data_list[i]["feat_dynamic_real"][0, :]
            feature_list.append(feature)

        ps_df = pd.DataFrame(
            {i: to_pandas(feature_list[i]) for i in range(self.n_stations)}
        ).interpolate()

        # Set the index to timestamp
        surge_df.index = surge_df.index.to_timestamp()
        ps_df.index = ps_df.index.to_timestamp()

        # Apply resampling for test data
        surge_df = surge_df.resample("1D").max()
        ps_df = ps_df.resample("1D").mean()

        surge_df, _, _ = normalize(
            surge_df, mean=train_metadata["surge_mean"], std=train_metadata["surge_std"]
        )
        ps_df, _, _ = normalize(
            ps_df, mean=train_metadata["ps_mean"], std=train_metadata["ps_std"]
        )

        # Create shifted data and PyTorch tensors
        predictor_ps_tensor, predictand_surge_tensor, date_tensor = (
            self.create_shifted_data_and_tensors(ps_df, surge_df)
        )
        # Create TensorDataset and DataLoader
        dataset = TensorDataset(
            predictand_surge_tensor, predictor_ps_tensor, date_tensor
        )
        loader = DataLoader(dataset, batch_size=512, shuffle=False)

        return dataset, loader

    def create_shifted_data_and_tensors(self, ps_df, surge_df):
        # Create shifted data for each station
        shifted_data = {
            f"station {i}": create_shifted_data(ps_df[i], self.n)
            for i in range(self.n_stations)
        }
        predictor_ps = {
            f"station {i}": shifted_data[f"station {i}"][self.n :]
            for i in range(self.n_stations)
        }
        predictand_surge = {
            f"station {i}": surge_df[i][self.n :] for i in range(self.n_stations)
        }
        # Convert to PyTorch tensors
        predictor_ps_tensor = torch.concat(
            [
                torch.tensor(predictor_ps[f"station {i}"].values, dtype=torch.float32)
                for i in range(self.n_stations)
            ],
            axis=1,
        )
        predictand_surge_tensor = torch.stack(
            [
                torch.tensor(
                    predictand_surge[f"station {i}"].values, dtype=torch.float32
                )
                for i in range(self.n_stations)
            ],
            axis=-1,
        )

        # Extract date information and convert to tensor
        _date_tensor = torch.tensor(surge_df.index[self.n :].astype(int).values)

        return predictor_ps_tensor, predictand_surge_tensor, _date_tensor


def normalize(data, method="z-score", mean=None, std=None):
    if mean is None:
        mean = data.mean()
    else:
        if not (type(data) is torch.Tensor):
            mean = mean.numpy()
    if std is None:
        std = data.std()
    else:
        if not (type(data) is torch.Tensor):
            std = std.numpy()
    normalized_data = (data - mean) / std
    return normalized_data, mean, std


def create_shifted_data(dataset, n):
    """
    Create shifted versions of a time series dataset for a specified number of previous time steps.

    Parameters:
    dataset (pd.Series or pd.DataFrame): The original time series or dataset to create shifted versions from.
    n (int): The number of previous time steps to use as predictors.

    Returns:
    pd.DataFrame: A DataFrame containing the shifted versions of the dataset.
    """
    shifted_data_dict = {"t": dataset}  # Current time step
    for i in range(1, n + 1):
        shifted_data_dict[f"t-{i}"] = dataset.shift(i)
    shifted_data = pd.DataFrame(shifted_data_dict)
    return shifted_data
