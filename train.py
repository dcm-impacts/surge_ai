import argparse
from datetime import datetime

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader

from surge_ai.data.dataset import CoastalWaterLevelDataset
from surge_ai.model import SurgePredictorLSTM
from surge_ai.train import train_model

# Argument Parser
parser = argparse.ArgumentParser(description="Train Surge Predictor Model")
parser.add_argument(
    "--sequence_length",
    type=int,
    default=72,
    help="Input sequence length for the model",
)
parser.add_argument(
    "--is_surge_feature", action="store_true", help="Include surge as a feature"
)
parser.add_argument(
    "--num_epochs", type=int, default=50, help="Number of epochs for training"
)
parser.add_argument(
    "--station_idx",
    type=int,
    default=0,
    help="Index of the station for which water levels are computed",
)
parser.add_argument("--climate_input", type=str, default="ERA5", help="Specify the input dataset for the climate data")

args = parser.parse_args()


dataset = CoastalWaterLevelDataset(
    "data/train/reanalysis_surge",
    f"data/train/{args.climate_input}/ps",
    f"data/train/{args.climate_input}/uas",
    f"data/train/{args.climate_input}/vas",
    sequence_length=args.sequence_length,
    station_idx=args.station_idx,
)

val_dataset = CoastalWaterLevelDataset(
    "data/val/reanalysis_surge",
    "data/val/ERA5/ps",
    "data/val/ERA5/uas",
    "data/val/ERA5/vas",
    sequence_length=args.sequence_length,
    station_idx=args.station_idx,
)

# Define DataLoader
train_loader = DataLoader(dataset=dataset, batch_size=32, shuffle=True)

# Define model
INPUT_SIZE = 4 if args.is_surge_feature else 3
model = SurgePredictorLSTM(
    input_size=INPUT_SIZE,
    hidden_size=24,
    num_layers=1,
    prediction_length=1,
    sequence_length=dataset.sequence_length,
)

# Define loss function and optimizer
criterion = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), lr=0.001)

# Train the model
train_model(model, train_loader, criterion, optimizer, num_epochs=args.num_epochs)

# Save the trained model
SURGE_FEATURE = "-surge_is_feature" if args.is_surge_feature else ""
current_time = datetime.now().strftime("%Y%m%d%H%M")
filename = (
    f"surge_predictor_lstm-seq_len{args.sequence_length}"
    f"{SURGE_FEATURE}"
    f"-epochs{args.num_epochs}"
    f"-station_idx_{args.station_idx}"
    f"-climate_input_{args.climate_input}"
    f"-{current_time}.pth"
)
torch.save(model.state_dict(), filename)
