## Input data for gtsm
taken from [ECMWF](https://confluence.ecmwf.int/display/CKB/Global+sea+level+change+time+series+from+1950+to+2050+derived+from+reanalysis+and+high+resolution+CMIP6+climate+projections%3A+Product+User+Guide#Globalsealevelchangetimeseriesfrom1950to2050derivedfromreanalysisandhighresolutionCMIP6climateprojections:ProductUserGuide-InputData)


2.3.3. Input Data
The global dataset of extreme sea levels described in this User Guide is based on the ERA5 climate reanalysis and the HighResMIP climate simulations. For the simulations of total water levels we force GTSM with wind and atmospheric pressure. More specially, the following variables are required: the mean sea level pressure (psl), zonal surface wind speed (uas), and meridional surface wind speed (vas). For each of these variables, we use a the highest temporal resolution available. For ERA5 this is hourly. For the HighResMIP ensemble, this is 3- or 6-hourly depending on the climate model.
