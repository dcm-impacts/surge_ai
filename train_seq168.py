import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader

from surge_ai.dataset import CoastalWaterLevelDataset
from surge_ai.model import SurgePredictorLSTM
from surge_ai.train import train_model

dataset = CoastalWaterLevelDataset(
    "data/train/reanalysis_surge",
    "data/train/ERA5/ps",
    "data/train/ERA5/uas",
    "data/train/ERA5/vas",
    sequence_length=168,
)

val_dataset = CoastalWaterLevelDataset(
    "data/val/reanalysis_surge",
    "data/val/ERA5/ps",
    "data/val/ERA5/uas",
    "data/val/ERA5/vas",
    sequence_length=168,
)

# Define DataLoader
train_loader = DataLoader(dataset=dataset, batch_size=32, shuffle=True)

# Define model
model = SurgePredictorLSTM(
    input_size=4,
    hidden_size=24,
    num_layers=1,
    prediction_length=1,
    sequence_length=dataset.sequence_length,
)

# Define loss function and optimizer
criterion = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), lr=0.001)

# Train the model
train_model(model, train_loader, criterion, optimizer, num_epochs=50)

# Save the trained model
torch.save(model.state_dict(), "surge_predictor_lstm_seq168.pth")
