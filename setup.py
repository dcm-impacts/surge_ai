from setuptools import setup, find_packages

setup(
    name="surge_ai",
    version="0.1.0",
    packages=find_packages(),
    description="A surrogate model for GTSM",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    author="Simon Treu",
    author_email="simon.treu@pik-potsdam.com",
    url="https://gitlab.pik-potsdam.de/dcm-impacts/surge_ai.git",
    install_requires=[
        # List your project dependencies here.
        # They will be installed by pip when your package is installed.
        "numpy",
        "torch",
        "xarray"
        # etc...
    ],
    classifiers=[
        # Choose your license as you wish
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
