# This is a dump for all the features I am planning
## General

* remove all gluonts dependencies and make my own DataLoader
## Package organization

* Make proper python package
* I would like to organize into different possible commands, i.e. 
    * train
    * inference
    * plot

## Storage management

* Store understandable checkpoints together with all information needed to 
reproduce the respective experiment
* Store the results of the inference step  


## Plotting

* Make plots a bit nicer
* Include map plots with
    * progression of the storm
    * some measure of the spatial dependencies
* Make a plot with stations on the x-axis and some statistical properties on the y axis
    * RMSE
    * 99th percentile
    * measure for distributional similarity


## General cleanups

* There is currently an intermix between my first LSTM model and the 
experiments I have done in Amsterdam


## Done
* Specify the location of the storage space close to 
* Enable to plot a particular time slice
* Make plotting possible for many (~100) stations
